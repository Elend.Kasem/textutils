package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + " ".repeat(extra) + text;

		}

		public String flushLeft(String text, int width) {
			int extra = (width - text.length()) / 2;
			return text + " ".repeat(extra) + " ".repeat(extra);
		}

		public String justify(String text, int width) {

			int noofspacetoadd = (width - text.length());

			StringBuilder justifiedLine = new StringBuilder();
			String[] words = text.split(" ");

			char[] chars = text.toCharArray();

			int count = 0;

			for (char c : chars) {
				if (c == ' ')
					count++;
			}

			int noofspacestoeachspace = (count / noofspacetoadd);

			String s = extracted(noofspacestoeachspace);

			for (String word : words) {
				justifiedLine.append(word).append(s);

			}

			return justifiedLine.toString();

		};
	};

	public String extracted(int noofspacestoeachspace) {
		String s = "";
		for (int i = noofspacestoeachspace; i > 0; i--) {
			s += " ";
		}
		return s;
	}

	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("foor", aligner.center("foor", 3));

	}

	@Test
	void testflushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
	}

	@Test
	void testflushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
	}

	@Test
	void testJustify() {
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
	}

}
